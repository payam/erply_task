<?php
/**
 * This is just a demonstration follow of which a Real Framework Does,
 * and I know that it can be Improved a Lot, I did`nt wanted to use full stack framework,
 * so, I decided to use some simple packages and a dispatch follow to made this work.
 *
 * thank you for your consideration.
 */
use Poirot\Router\Route;

use \Erply\Recruiting as Erply;

include_once __DIR__.'/.index.pre.php';


// Define Routes
//
$server
    ->handleRequest(
        new Route\RouteMethodSegment('product.form.insert', [
            'criteria' => '/products/new',
            'method'   => 'POST',
        ])
        , Erply\Actions\NewProductAction::class               // Handler
    )
    ->handleRequest(
        new Route\RouteMethodSegment('product.form.display', [
            'criteria' => '/products/new',
            'method'   => 'GET',
        ])
        , Erply\Actions\NewProductPage::class                // Handler
    )
    // Home
    ->handleRequest(
        new Route\RouteSegment('home', [
            'criteria' => '/',
        ])
        , function () {
            // Simple Welcome Message
            return '<h2>Welcome, Erply API Demonstration Server.</h2>'
                  .'<br/>'
                  .'<strong><a href="/products/new">Why not to add a product?</a></strong>';
        }
    )
;

## Listen To Requests and Dispatch
#
$server->run();
