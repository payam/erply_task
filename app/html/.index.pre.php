<?php
/**
 * This is just a demonstration follow of which a Real Framework Does,
 * and I know that it can be Improved a Lot, I did`nt wanted to use full stack framework,
 * so, I decided to use some simple packages and a dispatch follow to made this work.
 *
 * thank you for your consideration.
 */

use \Erply\Recruiting as Erply;


## Initializing Packages and Container Preparation -----------------------------------\
#
if (! file_exists(__DIR__.'/../vendor/autoload.php') )
    throw new \RuntimeException( "Unable to load Application.\n"
        . "- Type `composer install`; to install library dependencies.\n"
    );

if (! file_exists('/docker/initialized') )
    throw new \RuntimeException( "Initializing .....\n");


## Autoload Dependencies -------------------------------------------------------------\
#
require __DIR__.'/../vendor/autoload.php';

define('ERPLY_DIR_ROOT', dirname(__FILE__));

## Load Application ------------------------------------------------------------------\
#
// change cwd to the application root by default
chdir( ERPLY_DIR_ROOT );


// Build Server By Loading Dependencies From Config File
//
$config = Erply\Config\ArrayLoader::load(__DIR__.'/../config/sapi_default');
$server = new Erply\Server(
    new Erply\Server\BuildServer($config)
);

// Made services accessible from anywhere
IOC::GiveIoC( $server->services() );


// THIS IS HERE BECAUSE I DID`NT WANT TO USE MY OWN WORKER IMPLEMENTATION; AND I KNOW CODES HERE ARE UGLY
// because the library is designed with static methods and is not fit to service locator architect.
// !! I have used workers frequently on my projects and you can also take a look on those projects. thanks.
\Resque::setBackend(getenv('RMQ_HOST').':6379');
