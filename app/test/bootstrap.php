<?php
require_once __DIR__.'/../vendor/autoload.php';

require __DIR__  . '/../src/SplClassLoader.php';

$oClassLoader = new \SplClassLoader('Erply\\Recruiting', __DIR__ . '/../src');
$oClassLoader->register();
