<?php
namespace Erply\Recruiting\Services\Clients;

use Poirot\ApiClient\aClient;
use Poirot\ApiClient\Interfaces\iPlatform;
use Poirot\ApiClient\Interfaces\Request\iApiCommand;

use Poirot\Std\Struct\DataEntity;
use Poirot\Std\Interfaces\Struct\iDataEntity;

use Erply\Recruiting\Services\Clients\Erply\Command;
use Erply\Recruiting\Services\Clients\Erply\PlatformHttp;
use Erply\Recruiting\Services\Clients\Exceptions\exInvalidSession;


class ErplyClient
    extends aClient
{
    protected $username;
    protected $password;
    protected $clientCode;

    protected $serverUrl;
    protected $platform;

    protected $_session;
    protected $_isSessionExchanged;


    /**
     * Constructor.
     *
     * @param $username
     * @param $password
     * @param $clientCode
     */
    function __construct($username, $password, $clientCode)
    {
        $this->username   = $username;
        $this->password   = $password;
        $this->clientCode = $clientCode;
    }


    // Methods:

    /**
     * Save/Update Product
     *
     * @param Command\SaveProductOptions $options
     *
     * @return iDataEntity
     */
    function saveProduct(Command\SaveProductOptions $options)
    {
        $response = $this->call( new Command\SaveProduct($options) );
        if ( $ex = $response->hasException() )
            throw $ex;

        /** @var DataEntity $r */
        $r = $response->expected();
        return $r;
    }

    /**
     * Retrieve your product database
     *
     * @link https://learn-api.erply.com/requests/getproducts
     *
     * @return iDataEntity
     */
    function getProducts()
    {
        $response = $this->call( new Command\GetProducts );
        if ( $ex = $response->hasException() )
            throw $ex;

        /** @var DataEntity $r */
        $r = $response->expected();
        return $r;
    }

    /**
     * Returns a HIERARCHICAL array of product groups.
     *
     * @link https://learn-api.erply.com/requests/getproductgroups
     *
     * @return iDataEntity
     */
    function getProductGroups()
    {
        $response = $this->call( new Command\GetProductGroups );
        if ( $ex = $response->hasException() )
            throw $ex;

        /** @var DataEntity $r */
        $r = $response->expected();
        return $r;
    }

    /**
     * Verify Account
     *
     * @return iDataEntity
     */
    function verifyUser()
    {
        $response = $this->call( new Command\VerifyUser($this->username, $this->password) );
        if ( $ex = $response->hasException() )
            throw $ex;


        /** @var DataEntity $r */
        $r = $response->expected();
        return $r;
    }


    // Options:

    /**
     * Server Url
     *
     * @return string
     */
    function getServerUrl()
    {
        if (! $this->serverUrl )
            $this->serverUrl = 'https://'.$this->clientCode.'.erply.com/api/';


        return $this->serverUrl;
    }


    // Implement aClient

    /**
     * Get Client Platform
     *
     * - used by request to build params for
     *   server execution call and response
     *
     * @return iPlatform
     */
    protected function platform()
    {
        if (! $this->platform )
            $this->platform = new PlatformHttp;


        ## Default Options Overriding
        #
        $platform = $this->platform;
        if (! $platform->getServerUrl() )
            $platform->setServerUrl( $this->getServerUrl() );

        return $platform;
    }


    // ..

    /**
     * @inheritdoc
     */
    protected function call(iApiCommand $command)
    {
        $recall = 1;


        if ( $command instanceof Command\aCommand ) {
            if (! $command->getClientCode() )
                $command->setClientCode($this->clientCode);
        }


        recall:

        if ( method_exists($command, 'setSessionKey') )
        {
            // Object Is Session Aware
            //
            if (null === $session = $this->loadSession() )
                $session = $this->exchangeSession();


            $command->setSessionKey($session);
        }


        $response = parent::call($command);

        if ($ex = $response->hasException()) {

            if ( $ex instanceof exInvalidSession && $recall > 0 )
            {
                // Token revoked or mismatch
                // Refresh Session
                $this->exchangeSession();


                $recall--;
                goto recall;
            }


            throw $ex;
        }


        return $response;
    }


    // Exchanging Session Helpers:

    /**
     * Exchange New Session
     *
     * @return string
     */
    private function exchangeSession()
    {
        $this->_isSessionExchanged = true;

        $session = $this->verifyUser();
        $session = $session->get('records')[0]['sessionKey'];

        $this->saveSession($session);
        return $this->_session = $session;
    }

    private function loadSession()
    {
        if ($this->_isSessionExchanged)
            return $this->_session;


        if (! file_exists($this->_getTmpFilepath()) )
            return null;

        $content = file_get_contents( $this->_getTmpFilepath() );
        $session = unserialize($content);
        return $this->_session = $session;
    }

    private function saveSession($session)
    {
        file_put_contents($this->_getTmpFilepath(), serialize($session));
    }

    private function _getTmpFilepath()
    {
        return sys_get_temp_dir().'/session_'.$this->clientCode.'stored.dt';
    }
}
