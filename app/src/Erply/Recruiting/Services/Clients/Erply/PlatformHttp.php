<?php
namespace Erply\Recruiting\Services\Clients\Erply;

use Poirot\Psr7\Stream;
use Poirot\Psr7\HttpResponse;

use Poirot\ApiClient\aPlatform;
use Poirot\ApiClient\Interfaces\iPlatform;
use Poirot\ApiClient\Exceptions\exConnection;
use Poirot\ApiClient\Interfaces\Request\iApiCommand;

use Erply\Recruiting\Services\Clients\Erply\Command\aCommand;
use Erply\Recruiting\Services\Clients\Erply\PlatformHttp\ServerUrlEndpoints;


class PlatformHttp
    extends aPlatform
    implements iPlatform
{
    /** @var iApiCommand */
    protected $Command;

    // Options:
    protected $usingSsl  = false;
    protected $serverUrl = null;

    private $_headersSize = 0;


    // Alters

    protected function _SaveProduct(Command\SaveProduct $command)
    {
        $response = $this->makeRequestCall($command);
        $response = new Response($response);

        return $response;
    }

    protected function _GetProductGroups(Command\GetProductGroups $command)
    {
        $response = $this->makeRequestCall($command);
        $response = new Response($response);

        return $response;
    }

    protected function _GetProducts(Command\GetProducts $command)
    {
        $response = $this->makeRequestCall($command);
        $response = new Response($response);

        return $response;
    }

    protected function _VerifyUser(Command\VerifyUser $command)
    {
        $response = $this->makeRequestCall($command);
        $response = new Response($response);

        return $response;
    }


    // Options

    /**
     * Set Server Url
     *
     * @param string $url
     *
     * @return $this
     */
    function setServerUrl($url)
    {
        $this->serverUrl = (string) $url;
        return $this;
    }

    /**
     * Server Url
     *
     * @return string
     */
    function getServerUrl()
    {
        return $this->serverUrl;
    }

    /**
     * Using SSl While Send Request To Server
     *
     * @param bool $flag
     *
     * @return $this
     */
    function setUsingSsl($flag = true)
    {
        $this->usingSsl = (bool) $flag;
        return $this;
    }

    /**
     * Ssl Enabled?
     *
     * @return bool
     */
    function isUsingSsl()
    {
        return $this->usingSsl;
    }


    // ..

    /**
     * Send Request To Upstream Server
     *
     * @param aCommand $command
     *
     * @return HttpResponse
     */
    protected function makeRequestCall(aCommand $command)
    {
        $url = new ServerUrlEndpoints(
            $this->getServerUrl()
            , $command
            , $this->isUsingSsl()
        );

        $params = iterator_to_array($command);
        $params+= [
            'version'         => '1.0',
            'sendContentType' => '1',     // json
        ];


        //create request
        $handle = curl_init($url);

        curl_setopt($handle, CURLOPT_HEADERFUNCTION, function($ch, $h) { $this->_headersSize += strlen($h); return strlen($h); });

        //set the payload
        curl_setopt($handle, CURLOPT_POST, true);
        curl_setopt($handle, CURLOPT_POSTFIELDS, $params);

        //return body only
        curl_setopt($handle, CURLOPT_HEADER, 1);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, 1);

        //create errors on timeout and on response code >= 300
        curl_setopt($handle, CURLOPT_TIMEOUT, 45);
        curl_setopt($handle, CURLOPT_FAILONERROR, true);
        curl_setopt($handle, CURLOPT_FOLLOWLOCATION, false);

        //set up host and cert verification
        curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);
        if ( $this->isUsingSsl() ) {
            // TODO Implement SSL
            // ...
        }


        ## Send Request
        #
        $this->_headersSize = 0;

        $ret = \Poirot\Std\reTry(function () use ($handle, $url)
        {
            $ret = curl_exec($handle);

            if ( $curl_errno = curl_errno($handle) ) {
                // Connection Error
                $curl_error = curl_error($handle);
                $errorMessage = $curl_errno.':'.$curl_error.' '."When POST: $url";
                throw new exConnection($errorMessage, $curl_errno);
            }

            return $ret;

        }, 3, 1000);


        $httpResponse = $this->_buildFinalResponseFromResource($ret, $handle);

        curl_close($handle);

        return $httpResponse;
    }

    private function _buildFinalResponseFromResource($ret, $ch)
    {
        if (! is_resource($ch) || ! $ret)
            return false;


        $headers_size = $this->_headersSize;
        if (is_numeric($headers_size) && $headers_size > 0) {
            $headers = trim(substr($ret, 0, $headers_size));
            $body = substr($ret, $headers_size);
        } else
            list($headers, $body) = preg_split('/\r\n\r\n|\r\r|\n\n/', $ret, 2);
        if (! $body)
            $body = '';


        $header_lines = explode("\r\n", $headers);
        $headers = array();
        foreach ($header_lines as $header_line) {
            $buf = explode(': ', $header_line);
            if (count($buf) == 1)
                continue;

            $headers[$buf[0]] = $buf[1];
        }


        $stream = new Stream('php://memory', 'w+');
        $stream->write($body);
        $stream->rewind();

        $response = new HttpResponse($stream, curl_getinfo($ch, CURLINFO_HTTP_CODE), $headers);
        return $response;
    }
}
