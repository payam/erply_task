<?php
namespace Erply\Recruiting\Services\Clients\Erply\PlatformHttp;

use Poirot\ApiClient\Interfaces\Request\iApiCommand;


class ServerUrlEndpoints
{
    protected $serverBaseUrl;
    protected $command;


    /**
     * ServerUrlEndpoints constructor.
     *
     * @param $serverBaseUrl
     * @param $command
     */
    function __construct($serverBaseUrl, $command, $ssl = false)
    {
        $this->serverBaseUrl = (string) $serverBaseUrl;
        $this->command    = $command;
    }

    function __toString()
    {
        return $this->_getServerHttpUrlFromCommand($this->command);
    }


    // ..

    /**
     * Determine Server Http Url Using Http or Https?
     *
     * @param iApiCommand $command
     *
     * @return string
     * @throws \Exception
     */
    protected function _getServerHttpUrlFromCommand($command)
    {
        $base = null;

        $cmMethod = strtolower( (string) $command );
        switch ($cmMethod) {
            default:
                $base = '';
        }

        $serverUrl = rtrim($this->serverBaseUrl, '/');
        $serverUrl .= '/'. (($base) ? trim($base, '/').'/' : ''); // we need this trailing slash seriously
        return $serverUrl;
    }
}
