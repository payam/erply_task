<?php
namespace Erply\Recruiting\Services\Clients\Erply;

use Erply\Recruiting\Services\Clients\Exceptions\exInvalidSession;
use Poirot\ApiClient\HttpResponseOfClient;

use Poirot\ApiClient\Response\ExpectedJson;
use Poirot\Std\Interfaces\Struct\iDataEntity;

use Erply\Recruiting\Services\Clients\Exceptions\exServerError;


class Response
    extends HttpResponseOfClient
{
    /**
     * @inheritdoc
     */
    function doRecognizeErrorWithStatusCode($responseCode)
    {
        if ($responseCode != 200)
        {
            $errUrl = (! $this->getMeta('_url_') )
                ?: ', When calling '. $this->getMeta('_url_');

            throw new exServerError(
                sprintf('Server Respond With Error Status (%s) '.$errUrl, $responseCode)
                , $responseCode
            );
        }
    }

    /**
     * @inheritdoc
     */
    function doRecognizeErrorFromExpected($expected)
    {
        if (! $expected instanceof iDataEntity)
            throw new \Exception(sprintf(
                'Bad Data Response; get: (%s).'
                , \Poirot\Std\flatten($expected)
            ));


        if ( $status = $expected->get('status') )
        {
            if (isset($status['responseStatus']) && $status['responseStatus'] == 'error')
            {
                $errUrl = (! $this->getMeta('_url_') )
                    ?: ', When calling '. $this->getMeta('_url_');

                switch ($status['errorCode']) {
                    // Allow Client Http Catch Session Exception To Handle Exchange Token
                    case '1055':
                    case '1056':
                    case '1057':
                        throw new exInvalidSession(
                            sprintf('Supplied session key is invalid; Server Respond Error Code (%s); '.$errUrl, $status['errorCode'])
                            , 400
                        );
                        break;

                    default:
                        throw new exServerError(
                            sprintf('Request To Server Encounter Error Code (%s); '.$errUrl, $status['errorCode'])
                            , 400
                        );
                }
            }
        }
    }

    /**
     * @override because we have not relevant http header in response
     *
     * Always use json
     *
     *
     * @return ExpectedJson
     */
    function _getDataParser()
    {
        return new ExpectedJson;
    }

    /**
     * @override Erply does not change the response status when error happens
     *           So we have to parse and check response content to find error(s)
     *
     * Has Exception?
     *
     * @return \Exception|false
     */
    function hasException()
    {
        if ( $this->exception )
            // Already has exception
            return $this->exception;


        ## Try to detect response include exception?
        #
        try
        {
            $this->doRecognizeErrorWithStatusCode( $this->getResponseCode() );

            $expected = $this->expected();
            $this->doRecognizeErrorFromExpected( $expected );

        } catch (\Exception $e) {
            $this->exception = $e;
        }

        return $this->exception;
    }
}
