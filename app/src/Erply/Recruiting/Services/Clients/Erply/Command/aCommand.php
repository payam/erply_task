<?php
namespace Erply\Recruiting\Services\Clients\Erply\Command;

use Poirot\ApiClient\Interfaces\Request\iApiCommand;
use Poirot\ApiClient\Request\tCommandHelper;

use Poirot\Std\Hydrator\HydrateGetters;


abstract class aCommand
    implements iApiCommand
    , \IteratorAggregate
{
    use tCommandHelper;

    protected $clientCode;


    abstract function getRequest();


    function getClientCode()
    {
        return $this->clientCode;
    }

    function setClientCode($clientCode)
    {
        $this->clientCode = $clientCode;
    }

    /**
     * Use All Getter Methods as Traversable Attribute
     *
     * @inheritdoc
     */
    function getIterator()
    {
        $hyd = new HydrateGetters($this, [HydrateGetters::PROPERTY_CAMELCASE => true]);
        $hyd->setExcludeNullValues();

        return $hyd;
    }
}
