<?php
namespace Erply\Recruiting\Services\Clients\Erply\Command;


/**
 * Commands use this trait must accompanied with session key authentication token
 *
 * @see VerifyUser
 * @see PlatformHttp::_verifyUser
 */
trait tSessionAware
{
    protected $sessionKey;


    function setSessionKey($token)
    {
        $this->sessionKey = $token;
    }

    function getSessionKey()
    {
        return $this->sessionKey;
    }
}
