<?php
namespace Erply\Recruiting\Services\Clients\Erply\Command;


class SaveProduct
    extends aCommand
{
    use tSessionAware;

    /** @var SaveProductOptions */
    protected $options;


    /**
     * SaveProduct constructor.
     *
     * @param SaveProductOptions $options
     */
    function __construct(SaveProductOptions $options)
    {
        $this->options = $options;
    }


    function getRequest()
    {
        return 'saveProduct';
    }

    /**
     * @override attributes from option
     *
     * @inheritdoc
     */
    function getIterator()
    {
        // Some Exceptional Params that required
        //
        yield 'active'  => 1;
        yield 'groupID' => $this->options->getGroupId();

        yield from parent::getIterator();
        yield from $this->options->getIterator();
    }
}
