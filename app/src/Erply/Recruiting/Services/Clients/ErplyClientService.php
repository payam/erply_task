<?php
namespace Erply\Recruiting\Services\Clients;

use Poirot\Ioc\Container\Service\aServiceContainer;


class ErplyClientService
    extends aServiceContainer
{
    protected $clientCode;
    protected $username;
    protected $password;


    /**
     * Create Service
     *
     * @return mixed
     */
    function newService()
    {
        return new ErplyClient(
            $this->username
            , $this->password
            , $this->clientCode
        );
    }


    // ..

    function setClientCode($clientCode)
    {
        $this->clientCode = $clientCode;
    }

    function setUsername($username)
    {
        $this->username = $username;
    }

    function setPassword($password)
    {
        $this->password = $password;
    }
}
