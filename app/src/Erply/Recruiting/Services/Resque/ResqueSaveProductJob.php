<?php
namespace Erply\Recruiting\Services\Resque;

use Erply\Recruiting\Services\Clients\Erply\Command\SaveProductOptions;


class ResqueSaveProductJob
{
    public $args;


    function perform()
    {
        $erply = \IOC::GetIoC()->get('/clients/ErplyClient');
        $erply->saveProduct( new SaveProductOptions($this->args) );


        echo 'Product Create Successfully...';
    }
}
