<?php
namespace Erply\Recruiting\Services;

use Poirot\Ioc\Container\Service\aServiceContainer;


class ResqueService
    extends aServiceContainer
{
    protected $host;
    protected $port = '6379';


    /**
     * Create MQ Client
     *
     * @return \Resque
     */
    function newService()
    {
        \Resque::setBackend($this->host.':'.$this->port);
        return new \Resque;
    }


    // ..

    function setHost($host)
    {
        $this->host = $host;
    }

    function setPort($port)
    {
        $this->port = $port;
    }
}
