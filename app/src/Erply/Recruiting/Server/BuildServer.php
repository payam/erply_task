<?php
namespace Erply\Recruiting\Server;

use Poirot\Ioc\Container\BuildContainer;
use Poirot\Std\ConfigurableSetter;

use Erply\Recruiting\Server;


class BuildServer
    extends ConfigurableSetter
{
    protected $serviceLocator;


    /**
     * Build/Setup Application Options
     *
     * @param Server $app
     *
     * @return void
     */
    function build(Server $app)
    {
        ## Build Container Services
        #
        if ($this->serviceLocator) {
            $builder = new BuildContainer($this->serviceLocator);
            $builder->build( $app->services() );
        }

    }


    // Options:

    /**
     * @param mixed $serviceLocator
     */
    function setServiceLocator($serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }
}
