<?php
namespace Erply\Recruiting;


class ErplyDefined
{
    // Flash Message
    //
    const FLASH_NEW_PRODUCT = 'flash.product.new';


    // RabbitMQ
    //
    const MSG_QUEUE_CHANNEL = 'erply.queue.channel.default';
}
