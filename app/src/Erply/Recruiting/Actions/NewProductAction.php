<?php
namespace Erply\Recruiting\Actions;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

use Poirot\Http\Psr\ServerRequestBridgeInPsr;
use Poirot\Std\Exceptions\exUnexpectedValue;

use Erply\Recruiting\ErplyDefined;
use Erply\Recruiting\Forms\ProductHydrate;
use Erply\Recruiting\Services\Clients\ErplyClient;
use Erply\Recruiting\Services\Clients\Erply\Command\SaveProductOptions;
use Erply\Recruiting\Services\FlashMessage\FlashMessage;
use Erply\Recruiting\Services\Resque\ResqueSaveProductJob;


class NewProductAction
{
    /** @var ErplyClient */
    protected $erply;
    /** @var RequestInterface|ServerRequestBridgeInPsr */
    protected $request;
    /** @var ResponseInterface */
    protected $response;


    /**
     * Constructor.
     *
     * @param ErplyClient       $erply    @IoC /clients/ErplyClient
     * @param RequestInterface  $request  @IoC /HttpPsrRequest
     * @param ResponseInterface $response @IoC /HttpPsrResponse
     */
    function __construct(
        ErplyClient $erply
        , RequestInterface $request
        , ResponseInterface $response
    ) {
        $this->erply    = $erply;
        $this->request  = $request;
        $this->response = $response;
    }


    function __invoke()
    {
        # Hydrated/Validate Product From Http Request
        #
        $rData     = $this->request->getParsedBody();

        try {
            $hydEntity = new ProductHydrate($rData);
            $hydEntity->assertValidate();

        } catch (exUnexpectedValue $e) {

            ( new FlashMessage(ErplyDefined::FLASH_NEW_PRODUCT) )
                ->error( $e->getMessage() );


            return $this->resRedirect();
        }


        ## Check whether product with given name exists or not
        #
        $products = $this->erply->getProducts();
        foreach ($products->get('records') as $r)
        {
            if ( strtolower($r['name']) !== strtolower($hydEntity->getName()) )
                continue;


            (new FlashMessage(ErplyDefined::FLASH_NEW_PRODUCT))
                ->error(sprintf(
                    'Product with same name (%s) exists.'
                    , $hydEntity->getName()
                ));

            return $this->resRedirect();
        }


        ## Save Product / Notify Upstream Server
        #
        /** @var \Resque $r */
        $r = \IOC::GetIoC()->get('/clients/Resque');
        $r::enqueue(
            ErplyDefined::MSG_QUEUE_CHANNEL
            , ResqueSaveProductJob::class
            , iterator_to_array( new SaveProductOptions($hydEntity) )
        );


        ( new FlashMessage(ErplyDefined::FLASH_NEW_PRODUCT) )
            ->info( 'Product added Successfully.' );

        return $this->resRedirect();
    }

    // ..

    protected function resRedirect()
    {
        // TODO from router service instance we can assemble route uri from name (reverse route)
        return $this->response->withHeader('Location', '/products/new');
    }
}
