<?php
namespace Erply\Recruiting\Config;

use Poirot\Std\Glob;
use Poirot\Std\ErrorStack;
use Poirot\Std\Type\StdArray;


class ArrayLoader
{
    /**
     * Load Config Files From Given Directory
     *
     * - file name can be in form of dir/to/file then:
     *   it will looking for files
     *   "file.local.conf.php" and "file.conf.php"
     *
     * - if given argument is directory:
     *   load all files with extension
     *   ".local.conf.php" and ".conf.php"
     *
     *
     * @param string $path file or dir path
     *
     * @return StdArray|false
     */
    static function load($path)
    {
        $isLoaded = false;
        $config   = new StdArray;

        $globPattern = $path;
        if ( is_dir($path) ) {
            $globPattern = str_replace('\\', '/', $globPattern); // normalize path separator
            $globPattern = rtrim($globPattern, '/').'/*';
        }

        if (! is_file($path) )
            // did not given exactly name of file
            $globPattern .= '.{,local.}conf.php';

        foreach ( Glob::glob($globPattern, GLOB_BRACE) as $filePath ) {
            ErrorStack::handleException(function ($e) use ($filePath) {
                ob_end_clean();
                throw new \RuntimeException(
                    sprintf('Error while loading config: %s', $filePath)
                    , 0
                    , $e
                );
            });

            ErrorStack::handleError(E_ALL, function ($e){ throw $e; });

            ob_start();
            $fConf = include $filePath;
            if (! is_array($fConf) )
                throw new \RuntimeException(sprintf(
                    'Config file (%s) must provide array; given (%s).'
                    , $filePath
                    , \Poirot\Std\flatten($fConf)
                ));
            ob_get_clean();

            $config = $config->withMergeRecursive($fConf, false);

            ErrorStack::handleDone();
            ErrorStack::handleDone();

            $isLoaded |= true;
        }

        return ($isLoaded) ? $config : false;
    }
}
