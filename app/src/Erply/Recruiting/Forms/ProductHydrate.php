<?php
namespace Erply\Recruiting\Forms;

use Poirot\Std\tValidator;
use Poirot\Std\Hydrator\aHydrateEntity;
use Poirot\Std\Interfaces\Pact\ipValidator;
use Poirot\Std\Exceptions\exUnexpectedValue;


class ProductHydrate
    extends aHydrateEntity
    implements ipValidator
{
    use tValidator;

    protected $productGroup;
    protected $productName;
    protected $description;
    protected $longDescription;
    protected $price;
    protected $isTaxFree;
    protected $manufactureName;


    // Implement Validator:

    /**
     * Do Assertion Validate and Return An Array Of Errors
     *
     * @return exUnexpectedValue[]
     */
    function doAssertValidate()
    {
        $exceptions = [];

        if ( empty($this->productGroup) )
            $exceptions[] = exUnexpectedValue::paramIsRequired('group');

        if ( empty($this->productName) )
            $exceptions[] = exUnexpectedValue::paramIsRequired('name');

        if ( empty($this->description) )
            $exceptions[] = exUnexpectedValue::paramIsRequired('desc');

        if ( empty($this->price) )
            $exceptions[] = exUnexpectedValue::paramIsRequired('price');



        return $exceptions;
    }


    // Setter Options (Data Sent From Client; Usually Html Form)

    function setGroup($value)
    {
        $this->productGroup = $value;
    }

    function setName($value)
    {
        $this->productName = $value;
    }

    function setDesc($value)
    {
        $this->description = $value;
    }

    function setLndesc($value)
    {
        $this->longDescription = $value;
    }

    function setPrice($value)
    {
        $this->price = $value;
    }

    function setTxfree($value)
    {
        $this->isTaxFree = $value;
    }

    function setManname($value)
    {
        $this->manufactureName = $value;
    }


    // Hydration Getters:

    function getGroupID()
    {
        return (int) $this->productGroup;
    }

    function getName()
    {
        return $this->_assertNewLine( $this->_assertTrim($this->productName) );
    }

    function getDescription()
    {
        return $this->_assertNewLine( $this->_assertTrim($this->description) );
    }

    function getLongdesc()
    {
        return $this->_assertNewLine( $this->_assertTrim($this->longDescription) );
    }

    function getNetPrice()
    {
        return (float) $this->price;
    }

    function getTaxFree()
    {
        return ( isset($this->isTaxFree) );
    }

    function getManufacturerName()
    {
        return $this->_assertNewLine( $this->_assertTrim($this->manufactureName) );
    }


    // ..

    private function _assertNewLine($description)
    {
        return preg_replace( '/\t+/', '',  preg_replace('/(\r\n|\n|\r){3,}/', "$1$1", $description) );
    }

    private function _assertTrim($description)
    {
        return trim($description);
    }
}
