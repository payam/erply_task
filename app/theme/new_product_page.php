<?php
/**
 * Note: this is just simple page; Represent Simple Form
 *
 *       I`m not considering best practices in template renderer and dom documents
 *       such as base_url, assets, css design, etc...
 *
 * @var array $groups
 * @var array $flash_messages
 */
?>

<!DOCTYPE html>
<html class=no-js itemscope itemtype=https://schema.org/ItemList>

<head>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

    <style>

        ::placeholder {
            color: #555 !important;;
            opacity: 1; /* Firefox */
        }

        :-ms-input-placeholder { /* Internet Explorer 10-11 */
            color: #555 !important;;
        }

        ::-ms-input-placeholder { /* Microsoft Edge */
            color: #555 !important;;
        }

        label{
            position: relative;
            cursor: pointer;
            color: #666;
            font-size: 15px;
            margin-top: 5px;
        }

        input[type="checkbox"], input[type="radio"]{
            position: absolute;
            right: 9000px;
        }

        /*Check box*/
        input[type="checkbox"] + .label-text:before{
            content: "\f096";
            font-family: "FontAwesome";
            speak: none;
            font-style: normal;
            font-weight: normal;
            font-variant: normal;
            text-transform: none;
            line-height: 1;
            -webkit-font-smoothing:antialiased;
            width: 1em;
            display: inline-block;
            margin-right: 5px;
        }

        input[type="checkbox"]:checked + .label-text:before{
            content: "\f14a";
            color: #2980b9;
            animation: effect 250ms ease-in;
        }

        input[type="checkbox"]:disabled + .label-text{
            color: #aaa;
        }

        input[type="checkbox"]:disabled + .label-text:before{
            content: "\f0c8";
            color: #ccc;
        }

        .form_main {
            width: 100%;
        }
        .form_main h4 {
            font-family: roboto;
            font-size: 20px;
            font-weight: 300;
            margin-bottom: 15px;
            margin-top: 20px;
            text-transform: uppercase;
        }
        .heading {
            border-bottom: 1px solid #fcab0e;
            padding-bottom: 9px;
            position: relative;
        }
        .heading span {
            background: #9e6600 none repeat scroll 0 0;
            bottom: -2px;
            height: 3px;
            left: 0;
            position: absolute;
            width: 75px;
        }
        .form {
            border-radius: 7px;
            padding: 6px;
        }
        .txt[type="text"] {
            border: 1px solid #ccc;
            margin: 10px 0;
            padding: 10px 0 10px 15px;
            width: 100%;
        }

        .txt_3[type="text"] {
            margin: 10px 0 0;
            padding: 10px 0 10px 15px;
            width: 100%;
        }
        .txt_5[type="text"] {
            margin: 10px 0 0;
            padding: 10px 0 10px 15px;
            width: 100%;
            height: 120px;
        }
        .txt2[type="submit"] {
            background: #242424 none repeat scroll 0 0;
            border: 1px solid #4f5c04;
            border-radius: 25px;
            color: #fff;
            font-size: 16px;
            font-style: normal;
            line-height: 35px;
            margin: 20px 0;
            padding: 0;
            text-transform: uppercase;
            width: 30%;
        }
        .txt2:hover {
            background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
            color: #5793ef;
            transition: all 0.5s ease 0s;
        }
    </style>

</head>


<body>

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="form_main">
                    <h4 class="heading"><strong>Insert </strong> New Product <span></span></h4>

                    <?php foreach ($flash_messages as $type => $messages) { ?>
                        <?php foreach ($messages as $m) { ?>
                        <div class="alert <?= (($type == 'error') ? 'alert-danger' : 'alert-success') ?>" role="alert">
                            <?= $m['value'] ?>
                        </div>
                        <?php } ?>
                    <?php } ?>

                    <div class="form">
                        <form action="" method="post">
                            <select class="form-control txt" name="group">
                                <?php foreach ($groups as $g) { ?>
                                <option value="<?= $g['id'] ?>"><?= $g['name'] ?></option>
                                <?php } ?>
                            </select>

                            <input type="text" required="" placeholder="Product name" value="" name="name" class="form-control txt">
                            <textarea required="" placeholder="Product description" name="desc" type="text" class="form-control txt_3"></textarea>
                            <textarea placeholder="Long description of the product." name="lndesc" type="text" class="form-control txt_5"></textarea>
                            <input type="text" required="" placeholder="Manufacturer Name" value="" name="maname" class="form-control txt">

                            <input type="text" required="" placeholder="Net Price" value="" name="price" class="form-control txt">


                            <div class="form-check">
                                <label>
                                    <input class="form-control" type="checkbox" name="txfree" />
                                    <span class="label-text">Tax Free</span>
                                </label>
                            </div>

                            <input type="submit" value="submit" name="submit" class="txt2">
                        </form>
                    </div>
                </div>
            </div
        </div>
    </div>

</body>