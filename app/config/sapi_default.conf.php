<?php
/**
 * Default Sapi Application Options
 */
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

use Poirot\Router\Interfaces\iRouterStack;


return [

    'serviceLocator' => [
        ## Define The Concrete Implementations
        #
        'implementations' => [
            'HttpPsrRequest'  => RequestInterface::class,
            'HttpPsrResponse' => ResponseInterface::class,
            'Router'          => iRouterStack::class,

        ],

        ## Define Services
        #
        'services' => [
            'HttpPsrRequest'  => \Erply\Recruiting\Services\HttpRequestService::class,
            'HttpPsrResponse' => \Erply\Recruiting\Services\HttpResponseService::class,

            'Router'          => \Erply\Recruiting\Services\RouterService::class,
        ],

        'nested' => [
            'clients' => [
                'services' => [
                    'Resque'    => [
                        \Erply\Recruiting\Services\ResqueService::class,
                        'host' => getenv('RMQ_HOST'),
                    ],
                    'ErplyClient' => [
                        \Erply\Recruiting\Services\Clients\ErplyClientService::class,
                        'client_code' => getenv('ERPLY_CLIENT_CODE'),
                        'username'    => getenv('ERPLY_USERNAME'),
                        'password'    => getenv('ERPLY_PASSWORD'),
                    ],
                ],
            ],
        ],
    ],
];
